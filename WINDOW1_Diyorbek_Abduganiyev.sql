WITH ranked_customers AS (
  SELECT
    cust_id,
    channel_id,
    ROW_NUMBER() OVER (
      PARTITION BY channel_id
      ORDER BY SUM(amount_sold) DESC
    ) AS sales_rank
  FROM sh.sales
  WHERE EXTRACT(YEAR FROM time_id) IN (1998, 1999, 2001)
  GROUP BY cust_id, channel_id
)

SELECT
  rc.cust_id,
  rc.channel_id,
  TO_CHAR(SUM(s.amount_sold), 'FM999999999.00') AS formatted_total_sales
FROM sh.sales s
INNER JOIN ranked_customers rc ON rc.cust_id = s.cust_id
WHERE rc.sales_rank <= 300
  AND rc.channel_id = 4
GROUP BY rc.cust_id, rc.channel_id;
